#!/usr/bin/env bash

cargo fmt --all
cargo clippy --workspace --all-features
cargo test --workspace
