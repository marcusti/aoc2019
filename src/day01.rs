use std::fs::read_to_string;

pub fn run() {
    let numbers = read_data();
    println!("day01 part1: {}", part1(numbers.clone()));
    println!("day01 part2: {}", part2(numbers.clone()));
}

fn read_data() -> Vec<i32> {
    let data = read_to_string("data/day01.txt").expect("cannot read file");
    parse(&data).expect("cannot parse numbers")
}

fn parse(data: &str) -> Option<Vec<i32>> {
    data.trim().lines().map(|line| line.parse().ok()).collect()
}

fn calc(n: i32) -> i32 {
    n / 3 - 2
}

fn calc_recursive(n: i32) -> i32 {
    let c = calc(n);
    match c {
        _ if { c > 0 } => c + calc_recursive(c),
        _ => 0,
    }
}

fn part1(numbers: Vec<i32>) -> i32 {
    numbers.into_iter().map(calc).sum()
}

fn part2(numbers: Vec<i32>) -> i32 {
    numbers.into_iter().map(calc_recursive).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calc() {
        assert_eq!(calc(0), -2);
        assert_eq!(calc(12), 2);
        assert_eq!(calc(14), 2);
        assert_eq!(calc(1969), 654);
        assert_eq!(calc(100756), 33583);
    }

    #[test]
    fn test_calc_recursive() {
        assert_eq!(calc_recursive(0), 0);
        assert_eq!(calc_recursive(12), 2);
        assert_eq!(calc_recursive(14), 2);
        assert_eq!(calc_recursive(1969), 966);
        assert_eq!(calc_recursive(100756), 50346);
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(vec!(12, 14, 1969)), 2 + 2 + 654);
        assert_eq!(part1(read_data()), 3_223_398);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(vec!(12, 14, 1969)), 2 + 2 + 966);
        assert_eq!(part2(read_data()), 4_832_253);
    }
}
