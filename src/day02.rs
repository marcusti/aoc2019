use std::fs::read_to_string;

pub fn run() {
    let numbers = read_data();
    println!("day02 part1: {}", part1(numbers.clone(), 12, 2));
    println!("day02 part2: {}", part2(numbers.clone()));
}

fn read_data() -> Vec<usize> {
    let data = read_to_string("data/day02.txt").expect("cannot read file");
    parse(&data).expect("cannot parse numbers")
}

fn parse(data: &str) -> Option<Vec<usize>> {
    data.trim().split(',').map(|n| n.parse().ok()).collect()
}

fn transform(mut numbers: Vec<usize>) -> Vec<usize> {
    let mut i = 0;
    loop {
        if numbers[i] == 99 {
            break;
        }
        let opcode = numbers[i];
        let a = numbers[i + 1];
        let b = numbers[i + 2];
        let c = numbers[i + 3];
        match opcode {
            1 => numbers[c] = numbers[a] + numbers[b],
            2 => numbers[c] = numbers[a] * numbers[b],
            _ => break,
        }
        i += 4;
    }
    numbers
}

fn part1(mut numbers: Vec<usize>, a: usize, b: usize) -> usize {
    numbers[1] = a;
    numbers[2] = b;
    transform(numbers)[0]
}

fn part2(numbers: Vec<usize>) -> usize {
    for n in 0..99 {
        for m in 0..99 {
            let x = part1(numbers.clone(), n, m);
            if x == 19_690_720 {
                return 100 * n + m;
            }
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_transform() {
        assert_eq!(transform(vec!(1, 0, 0, 0, 99)), vec!(2, 0, 0, 0, 99));
        assert_eq!(transform(vec!(2, 3, 0, 3, 99)), vec!(2, 3, 0, 6, 99));

        assert_eq!(
            transform(vec!(2, 4, 4, 5, 99, 0)),
            vec!(2, 4, 4, 5, 99, 9801)
        );

        assert_eq!(
            transform(vec!(1, 1, 1, 4, 99, 5, 6, 0, 99)),
            vec!(30, 1, 1, 4, 2, 5, 6, 0, 99)
        );
    }

    #[test]
    fn test_part1() {
        assert_eq!(part1(read_data(), 12, 2), 4_484_226);
    }

    #[test]
    fn test_part2() {
        assert_eq!(part2(read_data()), 5696);
    }
}
